package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Record;
import com.example.demo.service.TradeService;

@RestController
@RequestMapping("/trading/api")
public class TradingController {
	@Autowired
	TradeService service;

	@GetMapping(value = "/")
	public List<Record> getAllRecords() {
		return service.getAllRecords();
	}

	@GetMapping(value = "/{id}")
	public Record getRecordById(@PathVariable("id") int id) {
		return service.getRecord(id);
	}

	@PostMapping(value = "/")
	public Record addRecord(@RequestBody Record record) {
		return service.newRecord(record);
	}

	@PutMapping(value = "/")
	public Record editRecordOpen(@RequestBody Record record) {
		return service.saveRecord(record);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteRecord(@PathVariable int id) {
		return service.deleteRecord(id);
	}
}
