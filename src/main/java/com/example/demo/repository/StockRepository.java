package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Record;

@Component
public interface StockRepository {

	public List<Record> getAllRecords();

	public Record getRecordById(int id);

	public Record editRecordOpen(Record stock);

	public int deleteRecord(int id);

	public Record addRecord(Record stock);

}
