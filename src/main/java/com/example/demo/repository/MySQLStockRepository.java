package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Record;

@Repository
public class MySQLStockRepository implements StockRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<Record> getAllRecords() {
		// TODO Auto-generated method stub
		String sql = "SELECT Id, StockTicker, Date, Open, High, Low, Close, Volume FROM stockprice";
		return template.query(sql, new StockRowMapper());

	}

	@Override
	public Record getRecordById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT Id, StockTicker, Date, Open, High, Low, Close, Volume FROM stockprice WHERE Id=?";
		return template.queryForObject(sql, new StockRowMapper(), id);
	}

	@Override
	public Record editRecordOpen(Record stock) {
		// TODO Auto-generated method stub
		String sql = "UPDATE stockprice SET StockOpen = ? " + "WHERE Id = ?";
		template.update(sql, stock.getStockTicker(), stock.getDate(), stock.getOpen(), stock.getHigh(), stock.getLow(),
				stock.getClose(), stock.getVolume());
		return stock;
	}

	@Override
	public int deleteRecord(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM stockprice WHERE Id = ?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Record addRecord(Record stock) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO stockprice(StockTicker, Date, Open, High, Low, Close, Volume) "
				+ "VALUES(?,?,?,?,?,?,?)";
		template.update(sql, stock.getStockTicker(), stock.getDate(), stock.getOpen(), stock.getHigh(), stock.getLow(),
				stock.getClose(), stock.getVolume());
		return stock;
	}

}

class StockRowMapper implements RowMapper<Record> {

	@Override
	public Record mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Record(rs.getInt("Id"), rs.getString("StockTicker"), rs.getString("Date"), rs.getDouble("Open"),
				rs.getDouble("High"), rs.getDouble("Low"), rs.getDouble("Close"), rs.getDouble("Volume"));
	}

}
