package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Record;
import com.example.demo.repository.StockRepository;

@Service
public class TradeService {
	@Autowired
	private StockRepository repository;

	public List<Record> getAllRecords() {
		return repository.getAllRecords();
	}

	public Record getRecord(int id) {
		return repository.getRecordById(id);
	}

	public Record saveRecord(Record record) {
		return repository.editRecordOpen(record);
	}

	public Record newRecord(Record record) {
		return repository.addRecord(record);
	}

	public int deleteRecord(int id) {
		return repository.deleteRecord(id);
	}
}
